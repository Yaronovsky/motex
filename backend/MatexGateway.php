<?php
namespace Matex;

use Matex\Traits\Crypt;


class MatexGateway implements MatexGatewayInterface
{
    use Crypt;

    private $connection;
    private $encKey;


    public function __construct(\PDO $connection, $encryptionKey, $encryptionNonce)
    {
        $this->connection = $connection;
        $this->encKey = $encryptionKey;
    }

    public function login($formData)
    {
        $query = 'SELECT pass, id, user_name 
            FROM `user_details`
                WHERE user_name = :username';
        $pdoEcexutionArray = [
            ':username' => $formData->username,
        ];
        $stmt = $this->connection->prepare($query);
        $stmt->execute($pdoEcexutionArray);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $this->decrypt( 
            $result['pass'], 
            $this->encKey
            ) === $formData->password 
                ? [$result['id'] => $result['user_name']] : false; 

    }

    public function register($formData)
    {
        // if($this->isDuplicateEmail($formData->email)) {
        //     return ['email' => 'email exists'];
        // };
        $password = $this->encrypt(
            $formData->password, 
            $this->encKey
        );
        $query = 'INSERT INTO `user_details` (`id`,`email`,`user_name`,`pass`)
            VALUES(null, :email, :username, :pass)';
        
        $stmt = $this->connection->prepare($query);
        
        $pdoEcexutionArray = [
            ':email' => $formData->email,
            ':username' => $formData->username,
            ':pass' => $password,
        ];
        return $stmt->execute($pdoEcexutionArray);
    }

    
    public function recover($formData)
    {
        
    }
}