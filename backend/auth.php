<?php
require_once '../backend/config.php';
require_once '../vendor/autoload.php';

use Matex\Validator\Validator;
use Matex\MatexDataType;
use Matex\MatexGateway;
use Matex\Usecases\{MatexLogin, MatexRegister};

session_start();

$formData = file_get_contents('php://input');
$formDataArray = json_decode($formData, true);
$formData = filter_var_array($formDataArray, FILTER_SANITIZE_STRING);

$connection = new PDO(
    DSN, 
    MYSQL_USER, 
    MYSQL_PASSWORD
);

$validator = new Validator($formData, $connection);
$validator->validate();
if (!$validator->isValid()) {
    die($validator->createJsonErrorsResponse());
}

$dataType = new MatexDataType($formData);


$gateway = new MatexGateway($connection, ENC_KEY, ENC_NONCE);

$usecaseName = '\Matex\Usecases\Matex' . $dataType->target; 
$usecase = new $usecaseName($gateway, $dataType);
$wasSuccessful = $usecase->execute();

if ($wasSuccessful && $formData['target'] === 'Login') {
    $_SESSION['auth'] = $wasSuccessful; 
    echo json_encode(['success' => true]);
    die();
}

echo json_encode(['success' => 1]);

// else if (!$actionStatus )