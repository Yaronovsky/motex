<?php
namespace Matex;

interface MatexGatewayInterface
{
    public function login($formData);
    public function register($formData);
    public function recover($formData);
}