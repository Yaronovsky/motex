<?php
namespace Matex\Usecases;

use Matex\MatexGatewayInterface;
use Matex\MatexDataType;

class MatexLogin
{
    private $gateway;
    private $dataType;

    public function __construct(MatexGatewayInterface $gateway, MatexDataType $dataType)
    {
        $this->gateway = $gateway;
        $this->dataType = $dataType;
    }

    public function execute()
    {
        return $this->gateway->login($this->dataType);
    }
}