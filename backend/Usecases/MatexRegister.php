<?php
namespace Matex\Usecases;

use Matex\{MatexDataType, MatexGatewayInterface};

class MatexRegister
{
    private $gateway;
    private $dataType;

    public function __construct(MatexGatewayInterface $gateway, MatexDataType $dataType)
    {
        $this->dataType = $dataType;
        $this->gateway = $gateway;    
    }

    public function execute() {
        return $this->gateway->register($this->dataType);
    }
}