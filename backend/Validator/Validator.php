<?php
namespace Matex\Validator;

use Matex\Traits\ValidationTraits;

class Validator
{
    use ValidationTraits;

    protected $username_limit = 5;
    protected $password_limit = 2;
    const EXCLUDED = ['repeatedPassword'];
    private $errorMessages = [
        'length' => 'length shoud be at least {length} characters long',
        'email' => 'wrong email format',
        'required' => 'required field',
        'passwordsDontMatch' => 'mismatch in passwords',
        'duplicateEmail' => 'email already exists'
    ];

    private $formData;
    private $errorsArray;
    private $connection;

    public function __construct($formData, \PDO $connection)
    {
        $this->formData = $formData;
        $this->connection = $connection;
    }

    public function validate()
    {
        $validationData = array_slice($this->formData, 1);
        
        foreach ($validationData as $key => $value) {
            if (!in_array($key, self::EXCLUDED)) {
                $this->errorsArray[] = $this->isFullfiled($value) 
                    ? null 
                    : [$key => $this->errorMessages['required']];

                $limitName = "{$key}_limit";

                if (isset($this->$limitName)) {
                    $this->errorsArray[] = $this->doesCorrespondToLength($value, $key) 
                        ? null
                        :  [$key => str_replace('{length}', $this->$limitName, $this->errorMessages['length'])];
                }
            }
        }

        if (isset($validationData['email'])) {
            $this->errorsArray[] = $this->isEmail($validationData['email']) 
                ? null
                : ['email' => $this->errorMessages['email']];

            $this->errorsArray[] = $this->isDuplicateEmail($validationData['email'], $this->connection) 
                ? ['email' => $this->errorMessages['duplicateEmail']]
                : null;
        }

        if (isset($validationData['password']) && isset($validationData['repeatedPassword'])) {
            $this->errorsArray[] = $this->areStringsSame(
                [
                    $validationData['password'],
                    $validationData['repeatedPassword']
                ]
            ) 
                ? null
                : ['repeatedPassword' => $this->errorMessages['passwordsDontMatch']];
        }
        $this->clean();
    }

    public function isValid() {
        $errors = array_reduce($this->errorsArray, 'array_merge', []);
        return count(array_filter($errors)) ? 0 : 1;
    }

    private function clean() {
        $this->errorsArray = array_values(array_filter($this->errorsArray));
    }
    public function createJsonErrorsResponse() {
        return json_encode($this->errorsArray);
    }
}