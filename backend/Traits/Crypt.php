<?php
namespace Matex\Traits;

trait Crypt
{
    public function encrypt($text, $key)
    {
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        return base64_encode($nonce . sodium_crypto_secretbox($text, $nonce, $key));
    }
    
    public function decrypt($password, $key)
    {
        $decoded = base64_decode($password);
        $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $cipheredText =  mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');
        return sodium_crypto_secretbox_open($cipheredText, $nonce, $key);
    }



}