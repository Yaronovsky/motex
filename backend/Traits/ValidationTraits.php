<?php
namespace Matex\Traits;

trait ValidationTraits
{
    public function isFullfiled($field)
    {
        return !empty($field);
    }

    public function isEmail($field) {
        return filter_var($field, FILTER_VALIDATE_EMAIL) ? 1 : 0;
    }

    public function doesCorrespondToLength($field, $key) {
        $limit = $key. '_limit';
        if (isset($this->$limit)) {
            return $this->$limit < strlen($field);
        }
    }

    public function areStringsSame($stringsArray) {
        return strcmp(...$stringsArray) ? 0 : 1;
    }

    public function isDuplicateEmail($email, $connection) {
        $query = 'SELECT 1 FROM user_details WHERE email = :email';
        
        $stmt = $connection->prepare($query);
        
        $pdoEcexutionArray = [
            ':email' => $email,
        ];
        $stmt->execute($pdoEcexutionArray);
        return $stmt->fetch();
    }



}