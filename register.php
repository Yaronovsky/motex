<div class="Bg-main Bg-register">
        <div class="Register-block">
            <div class="Login-form-group">
                <form class="Register-form">
                    <label for="">
                        <input type="text" name="username" placeholder="Name">
                    </label>
                    <label for="">
                        <input class="u-margin-top-md" type="text" name="email" placeholder="Email">
                    </label>
                    <label for="">
                        <input class="u-margin-top-md" type="password" name="password" placeholder="Password">
                    </label>
                    <label for="">
                        <input class="u-margin-top-md" type="password" name="repeatedPassword" placeholder="Confirm password">
                    </label>
                    <label class="u-flex-horizontal u-margin-top-md" for="remember">
                        <input name="terms" id="remember" type="checkbox">
                        <span>I accept Terms and conditions</span>
                    </label>
                </form>
                <button class="Register-btn"></button>
                <div class="Matex-form-footer">
                    <span class="already-have"></span>
                    <a class="login-anchor" href="./?path=login">Click me to Login</a>
                </div>
            </div>
        </div>
    </div>
    teatat