<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../motex/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <script src="../motex/js/index.js"></script>
    <title>Document</title>
</head>
<body>
    <?php
    session_start();
    if (!isset($_SESSION['auth'])) {
        $path = isset($_GET['path']) ? $_GET['path'] : null;
        switch ($path) {
            case 'login':
                require_once 'login.php';
                    break;
            case 'recover':
                require_once 'recover.php';
                    break;
            default:
            require_once 'register.php';
        }
    }
    else {
        require_once 'loggedIn.php';
    }
    ?>
</body>
</html>