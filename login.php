<div class="Bg-main Bg-login">
    <div class="Login-block">
        <div class="Login-form-group">
            <form class="Login-form">
                <label for="">
                    <input type="text" name="username" placeholder="Username">
                </label>
                <label for="">
                    <input class="u-margin-top-md" type="text" name="password" placeholder="Password">
                </label>
            </form>
            <label class="u-flex-horizontal u-margin-top-md" for="remember">
                <input id="remember" type="checkbox">
                <span>Remember password</span>
            </label>
            <button class="Login-btn"></button>
            <div class="Matex-form-footer">
                <span class="already-have"></span>
                <a class="login-anchor u-margin-top-sm" href="./?path=register">Click me to create account</a>
                <a class="login-anchor u-margin-top-sm" href="./?path=recover">I forgot my password</a>
            </div>
        </div>
    </div>
</div>