<div class="Bg-main Bg-recover">
        <div class="Register-block">
            <div class="Login-form-group">
                <form class="Recover-form">
                    <label for="">
                        <input class="u-margin-top-md" type="text" name="email" placeholder="Email">
                    </label>
                </form>
                <button class="Recover-btn"></button>
                <div class="Matex-form-footer">
                    <a class="login-anchor" href="./?path=login">Back to Login</a>
                </div>
            </div>
        </div>
    </div>