class FormSender {
    constructor() {
        this.url = 'backend/auth.php';
        const formLocation = location.search.split('=').pop();
        this.formModuleName = `${formLocation.charAt(0).toUpperCase()}${formLocation.substr(1)}`
            || 'Register';
        this.attachSubmissionEvent();
        this.submitForm = this.submitForm.bind(this);
    }

    attachSubmissionEvent() {
        window.addEventListener('load', () => {
            const submitButton = document.querySelector(
                `.${this.formModuleName}-btn`
            );
            submitButton.addEventListener('click', this.submitForm);
        });
    }

    async submitForm() {
        this.removeErrors();
        const form = document.querySelector(`.${this.formModuleName}-form`);
        let formData = this.parseForm(form);
            formData = JSON.stringify(formData);
        let response = await fetch(this.url, {
            body: formData,
            method: 'POST'
        });
        let parsedResponse = await response.json();
        if (!('success' in parsedResponse)) {
            this.generateErrors(parsedResponse);
            return;
        }
        this[`onSuccessfull${this.formModuleName}`](Object.keys(parsedResponse)[0]);
    }

    parseForm(form) {
        return Array.from(form).reduce((accumulator, current) => {
            accumulator[current.name] = current.type !== 'checkbox' 
                ? current.value 
                : current.checked;
            return accumulator;
        }, {target: this.formModuleName})
    }

    generateErrors(errorsModel) {
        errorsModel.forEach(element => {
            console.log(element);
            const errorMessageText = Object.values(element)[0];
            let errorMessage = document.createElement('span');
                errorMessage.className = 'error';
                errorMessage.textContent = errorMessageText;

            let input = document.querySelector(`[name=${Object.keys(element)[0]}]`).parentNode;
                input.after(errorMessage);
        });
    }

    onSuccessfullLogin() {
        window.location.href ="./?path=loggedIn"; 
    }

    createLoggedInDialog() {
        (`Bg-${this.formLocation}`)
    }

    onSuccessfullRegister(message) {
        document.querySelector(`.${this.formModuleName}-form`).reset();
        let messageElement = document.createElement('span');
            messageElement.textContent = message;
            messageElement.className = 'success';
        document.querySelector(`.${this.formModuleName}-btn`).parentElement.after(messageElement);
        setTimeout(() => {
            messageElement.remove();
        }, 3000);
    }

    removeErrors() {
        let errors = Array.from(document.querySelectorAll('span.error'));
            errors.forEach(error => error.remove());
    }
}

new FormSender();

